#!/bin/bash
#step one, install java
yum install -y java-1.7.0-openjdk-devel

#install maven on centos

#get the file and unzip to /usr/local
wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar xzf apache-maven-3.3.9-bin.tar.gz -C /usr/local

# create the symbolic links
ln -s /usr/local/apache-maven-3.3.9 /usr/local/maven
ln -s /usr/local/maven/bin/mvn /bin/mvn
