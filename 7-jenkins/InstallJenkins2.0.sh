#!/bin/bash

#install git for Jenkins and Java for maven
yum install -y git java-1.7.0-openjdk-devel

#install maven - then will auto-install the maven
#integration plugin
wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar xzf apache-maven-3.3.9-bin.tar.gz -C /usr/local
ln -s /usr/local/apache-maven-3.3.9 /usr/local/maven
ln -s /usr/local/maven/bin/mvn /bin/mvn

#install jenkins
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
yum install -y jenkins
service jenkins start
